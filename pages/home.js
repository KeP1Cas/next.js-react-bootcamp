import MainContainer from "../components/MainContainer";
import firebase from "../firebase/clientApp";

import { useAuthState } from "react-firebase-hooks/auth";

const Index = () => {
  const [user, loading, error] = useAuthState(firebase.auth());

  console.log("Loading: ", loading, "|", "Current user: ", user);

  return (
    <MainContainer keywords={"main page"}>
      <h1>Главная страница</h1>
    </MainContainer>
  );
};

export default Index;
