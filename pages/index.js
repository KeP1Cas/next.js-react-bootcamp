import React from "react";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import MainContainer from "../components/MainContainer";
import firebase from "../firebase/clientApp";
import A from "../components/A";

const uiConfig = {
  signInSuccessUrl: "/users",
  signInOptions: [firebase.auth.GoogleAuthProvider.PROVIDER_ID],
};

function SignInScreen() {
  return (
    <MainContainer>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          position: "absolute",
          width: "100%",
          top: "30%",
          textAlign: "center",
        }}
      >
        <h1>Next.js Login</h1>
        <p>Вход для Приватного доступа</p>
        <StyledFirebaseAuth
          uiConfig={uiConfig}
          firebaseAuth={firebase.auth()}
        />

        <button>
          <A href={"/home"} text="Вход в публичный доступ " />
        </button>
      </div>
    </MainContainer>
  );
}

export default SignInScreen;
