import Head from "next/head";
import A from "./A";

const MainContainer = ({ children, keywords }) => {
  return (
    <>
      <Head>
        <meta
          keywords={"nextjs-reactbootcamp, react-bootcamp, nextjs, " + keywords}
        ></meta>
        <title>Next.js React-Bootcamp</title>
      </Head>
      <div className="navbar"></div>
      <div>{children}</div>
      <style jsx>
        {`
          .navbar {
            background: #00856f;
            padding: 15px;
            text-align: center;
          }
        `}
      </style>
    </>
  );
};

export default MainContainer;
